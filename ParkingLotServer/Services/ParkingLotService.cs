using System;
using System.Linq;

namespace ParkingLotServer.Services
{
    public class ParkingLotService : IParkingLotService
    {
        public double Balance => ParkingLot.Instance.Balance;
        public int FreePlaces => (ParkingLotSettings.MaxVehicles - ParkingLot.Instance.Vehicles.Count);
        public double LastMinuteEarnings => new TransactionsService().GetLast(TimeSpan.FromMinutes(1)).Sum(t => t.Amount);
    }
}