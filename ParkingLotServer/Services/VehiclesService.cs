using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace ParkingLotServer.Services
{
    public class VehiclesService : IVehiclesService
    {
        public void Add(Vehicle vehicle)
        {
            ParkingLot.Instance.AddVehicle(vehicle);
        }

        public List<Vehicle> GetAll()
        {
            return ParkingLot.Instance.Vehicles;
        }

        public void Remove(Vehicle vehicle)
        {
            ParkingLot.Instance.Vehicles.Remove(vehicle);
        }
    }

    public interface IVehiclesService
    {
        void Add(Vehicle vehicle);
        List<Vehicle> GetAll();
        void Remove(Vehicle vehicle);
    }
}