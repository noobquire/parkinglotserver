﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Timer = System.Timers.Timer;

namespace ParkingLotServer.Services
{
    public sealed class ParkingLot
    {
        private static readonly object SLock = new object();
        private static ParkingLot _instance;
        public double Balance { get; set; }
        public event EventHandler PaymentRequest;
        public readonly Timer PaymentTimer;
        private ITransactionsService _transactionService;

        public List<Transaction> LastTransactions => _transactionService.GetLast(TimeSpan.FromMinutes(1));
        public List<Transaction> AllTransactions => _transactionService.GetAll();

        public double EarnedLastMinute => LastTransactions.Sum(t => t.Amount);

        private ParkingLot()
        {
            
            PaymentTimer = new Timer(ParkingLotSettings.ChargePeriod * 1000);
            PaymentTimer.Elapsed += (s, e) => OnPaymentRequest();
            PaymentTimer.Start();
            _transactionService = new TransactionsService();

            Vehicles = new List<Vehicle>();
        }

        public void RemoveVehicle(Vehicle vehicle)
        {
            foreach (var currVehicle in Vehicles)
            {
                if (currVehicle.Type == vehicle.Type && currVehicle.Name == vehicle.Name)
                {
                    PaymentRequest -= vehicle.Charge;
                    Vehicles.Remove(vehicle);
                    break;
                }
            }

        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count + 1 < ParkingLotSettings.MaxVehicles)
            {

                PaymentRequest += vehicle.Charge;
                Vehicles.Add(vehicle);
            }
            else
            {
                throw new ArgumentException("Parking lot is full");
            }
        }

        public readonly List<Vehicle> Vehicles;

        public static ParkingLot Instance
        {
            get
            {
                if (_instance != null) return _instance;
                Monitor.Enter(SLock);
                ParkingLot temp = new ParkingLot();
                Interlocked.Exchange(ref _instance, temp);
                Monitor.Exit(SLock);
                return _instance;
            }
        }
        private void OnPaymentRequest()
        {
            PaymentRequest?.Invoke(this, EventArgs.Empty);
        }
    }
}
