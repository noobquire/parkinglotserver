namespace ParkingLotServer.Services
{
    public enum VehicleType
    {
        Car,
        Bus,
        Truck,
        Motorcycle
    }
}