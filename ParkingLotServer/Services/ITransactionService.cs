using System;
using System.Collections;
using System.Collections.Generic;

namespace ParkingLotServer.Services
{
    public interface ITransactionsService
    {
        List<ParkingLotServer.Services.Transaction> GetLast(TimeSpan timeFromNow);
        List<ParkingLotServer.Services.Transaction> GetAll();
    }
}