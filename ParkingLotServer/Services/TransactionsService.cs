using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace ParkingLotServer.Services
{
    public class TransactionsService : ITransactionsService
    {
        public List<ParkingLotServer.Services.Transaction> GetLast(TimeSpan timeFromNow)
        {
            return GetAll().Where(t => DateTime.Now - t.TimePayed < timeFromNow).ToList();
        }
        public List<Transaction> GetAll()
        {
            if (!File.Exists(TransactionLogger.LogPath)) return new List<Transaction>();
            List<Transaction> list;
            using (var sr = new StreamReader(TransactionLogger.LogPath))
            {
                list = JsonConvert.DeserializeObject<List<Transaction>>(sr.ReadToEnd());
            }

            return list;
        }
    }
}