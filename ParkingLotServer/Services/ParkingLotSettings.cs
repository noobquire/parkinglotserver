﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLotServer.Services
{
    [Serializable]
    public static class ParkingLotSettings
    {
        public static double InitialBalance = 0;
        public static int MaxVehicles = 10;
        public static double ChargePeriod = 5;
        public static double OverdueFineCoefficient = 1.5;

        public static double CarFee = 2;
        public static double TruckFee = 5;
        public static double BusFee = 3.5;
        public static double MotorcycleFee = 1;
    }
}
