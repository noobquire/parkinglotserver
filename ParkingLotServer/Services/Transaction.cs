﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace ParkingLotServer.Services
{
    [Serializable]
    public sealed class Transaction
    {
        [JsonProperty("payer")]
        public Vehicle Payer { get; set; }
        [JsonProperty("timePayed")]
        public DateTime TimePayed { get; set; }
        [JsonProperty("amount")]
        public double Amount { get; set; }

        public Transaction(Vehicle payer, DateTime time, double amount)
        {
            Payer = payer;
            TimePayed = time;
            Amount = amount;
        }

        public override string ToString()
        {
            return $"Transaction {Amount} at {TimePayed:dd.MM.yyyy HH:mm:ss} from {Payer}";
        }
    }
}
