namespace ParkingLotServer.Services
{
    public interface IParkingLotService
    {
        double Balance { get; }
        int FreePlaces { get; }
        double LastMinuteEarnings { get; }
    }
}