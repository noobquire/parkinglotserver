﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace ParkingLotServer.Services
{
    public static class TransactionLogger
    {
        
        public static string LogPath = Environment.CurrentDirectory + @"\Transactions.log";
        public static void Log(Transaction transaction)
        {
            var allTransactions = new TransactionsService().GetAll();
            allTransactions.Add(transaction);
            using (StreamWriter sw = new StreamWriter(LogPath, false))
            {
                sw.Write(JsonConvert.SerializeObject(allTransactions));
            }
        }
    }
}
