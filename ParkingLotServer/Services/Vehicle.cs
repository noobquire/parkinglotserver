﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ParkingLotServer.Services
{
    [Serializable]
    public class Vehicle
    {
        [JsonProperty("balance")] public double Balance { get; private set; }
        [JsonProperty("name")] public string Name { get; }

        [JsonProperty("type")] [JsonConverter(typeof(StringEnumConverter))]
        public readonly VehicleType Type;
        private double Fee
        {
            get
            {
                double fee = 0;
                switch (Type)
                {
                    case VehicleType.Car:
                        fee = ParkingLotSettings.CarFee;
                        break;
                    case VehicleType.Bus:
                        fee = ParkingLotSettings.BusFee;
                        break;
                    case VehicleType.Truck:
                        fee = ParkingLotSettings.TruckFee;
                        break;
                    case VehicleType.Motorcycle:
                        fee = ParkingLotSettings.MotorcycleFee;
                        break;
                }

                return fee;
            }
        }

        public Vehicle(string name, VehicleType type)
        {
            Name = name;
            Type = type;
            Balance = 0;
        }

        public void RechargeBalance(double amount)
        {
            if (amount > 0)
            {
                Balance += amount;
            }
        }
        /*
        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Name.GetHashCode();
            hash = (hash * 7) + Type.GetHashCode();
            return hash;
        }
        */

        public override string ToString()
        {
            return $"{Type} {Name}";
        }

        public void Charge(object sender, EventArgs args)
        {
            if (Balance < Fee)
            {
                Balance -= Fee * ParkingLotSettings.OverdueFineCoefficient;
                ParkingLot.Instance.Balance += Fee * ParkingLotSettings.OverdueFineCoefficient;
            }

            Balance -= Fee;
            ParkingLot.Instance.Balance += Fee;
            TransactionLogger.Log(new Transaction(this, DateTime.Now, Fee));
        }
    }
}