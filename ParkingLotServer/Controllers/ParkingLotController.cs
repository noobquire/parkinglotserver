using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ParkingLotServer.Services;

namespace ParkingLotServer.Controllers
{
    [Route("parkinglot")]
    [ApiController]
    public class ParkingLotController : ControllerBase
    {
        private readonly IParkingLotService _parkingLotService;

        public ParkingLotController()
        {
            _parkingLotService = new ParkingLotService();
        }

        [HttpGet]
        [Route("balance")]
        public ActionResult<double> Balance()
        {
            return Ok(_parkingLotService.Balance);
        }

        [HttpGet]
        [Route("freeplaces")]
        public ActionResult<int> FreePlaces()
        {
            return Ok(_parkingLotService.FreePlaces);
        }

        [HttpGet]
        [Route("lastminuteearnings")]
        public ActionResult<double> LastMinuteEarnings()
        {
            return Ok(_parkingLotService.LastMinuteEarnings);
        }
    }
}