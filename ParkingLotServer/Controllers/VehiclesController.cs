using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ParkingLotServer.Services;

namespace ParkingLotServer.Controllers
{
    [Route("vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly VehiclesService _vehiclesService;

        public VehiclesController()
        {
            _vehiclesService = new VehiclesService();
        }

        // GET /vehicles/
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetAll()
        {
            return Ok(_vehiclesService.GetAll());
        }
        
        
        [HttpPost]
        public ActionResult Add([FromBody]Vehicle vehicle)
        {
            _vehiclesService.Add(vehicle);
            return Ok();
        }
        
        [HttpDelete]
        public ActionResult Remove([FromBody]Vehicle vehicle)
        {
            _vehiclesService.Remove(vehicle);
            return Ok();
        }
    }
}