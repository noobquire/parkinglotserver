using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ParkingLotServer.Services;

namespace ParkingLotServer.Controllers
{
    [Route("transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionsService _transactionsService;

        public TransactionsController()
        {
            _transactionsService = new TransactionsService();
        }
        
        // GET /transactions
        [HttpGet]
        public ActionResult<IEnumerable<Transaction>> GetAll()
        {
            return Ok(_transactionsService.GetAll());
        }
        
        // GET /transactions/last/60
        [HttpGet("{seconds:int}")]
        [Route("last/{seconds}")]
        public ActionResult<IEnumerable<Transaction>> GetLast(int seconds)
        {
            return Ok(_transactionsService.GetLast(TimeSpan.FromSeconds(seconds)));
        }
    }
}