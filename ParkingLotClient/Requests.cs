using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ParkingLotServer.Services;

namespace ParkingLotClient
{
    public static class Requests
    {
        public static double GetParkingLotBalance()
        {
            var uri = "https://localhost:5001/parkinglot/balance/";
            return double.Parse(GetAsync(uri).Result);
        }

        public static int GetParkingLotFreePlaces()
        {
            var uri = "https://localhost:5001/parkinglot/freeplaces/";
            return int.Parse(GetAsync(uri).Result);
        }

        public static double GetLastMinuteEarnings()
        {
            var uri = "https://localhost:5001/parkinglot/lastminuteearnings/";
            return double.Parse(GetAsync(uri).Result);
        }

        public static List<Transaction> GetLastTransactions(TimeSpan timeFromNow)
        {
            return GetAllTransactions().Where(t => DateTime.Now - t.TimePayed <= timeFromNow).ToList();
        }

        public static List<Transaction> GetAllTransactions()
        {
            var uri = "https://localhost:5001/transactions/";
            return JsonConvert.DeserializeObject<List<Transaction>>(GetAsync(uri).Result);
        }

        public static List<Vehicle> GetAllVehicles()
        {
            var uri = "https://localhost:5001/vehicles/";
            return JsonConvert.DeserializeObject<List<Vehicle>>(GetAsync(uri).Result);
        }

        public static void AddVehicle(Vehicle vehicle)
        {
            var uri = "https://localhost:5001/vehicles/";
            PostVehicleAsync(vehicle, uri);
        }
        
        
        private static async Task<string> GetAsync(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using(HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using(Stream stream = response.GetResponseStream())
            using(StreamReader reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync();
            }
        }
        private static async Task<HttpResponseMessage> PostVehicleAsync(Vehicle content, string uri)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, uri))
            {
                var json = JsonConvert.SerializeObject(content);
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    Console.WriteLine(stringContent.ReadAsStringAsync().Result);
                    return await client.PostAsync(uri, stringContent);
                }
            }
        }
    }
}